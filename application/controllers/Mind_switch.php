<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mind_switch extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        $this->load->model('report_model');
        $this->load->model('mind_switch_model');
		
    }
	public function index()
 {


 
  $items = $this->input->post('items');
   
  $userId = $this->input->post('userId');
  $testdate = $this->input->post('testdate');
  $target   = $this->input->post('personalgoal');
 
  $testId = $this->input->post('testId');
  $end_date = date('Y-m-d', strtotime('+3 month', strtotime($testdate)));
  
  //$array=json_decode($items); 
   // $postdata = json_decode($array);
   $c_date= date('Y-m-d', strtotime('-3 month', strtotime($testdate)));
    $postdata=json_decode($items); 

  foreach($postdata as $values)  
  {
   $post_data = array('r_test_item_id' => $values->testItemId,
        'value' => $values->value,
        'note' => $values->note,
        'meas_value' => $values->measValue,
        'test_score'=> $values->testScore,
        'refer' => $values->refer,
        'score' => $values->score,
        'r_user_id' => $userId,
        'r_test_id' => $testId,
        'test_date' => $testdate,
        'test_end_date'=>$end_date
        );

    $ins_id = $this->mind_switch_model->insert_data($post_data);
  }
 
	$per = $this->mind_switch_model->personal_goal($userId,$target); 
  /*******for correct level**********/
    $vita_16 = $this->report_model->get_vita_sixteeen($userId);
    $except_vita_16 = $this->report_model->get_stress_sleep_mindfullness_level($userId);
    if($vita_16<=1)
    {
      $vita_16=1;
    }
    else if($vita_16>=7)
    {
      $vita_16=7;
    }
    if( $except_vita_16['stress']<=1)
    {
       $except_vita_16['stress']=1;
    }
    else if($except_vita_16['stress']>=7)
    {
       $except_vita_16['stress']=7;
    }
     if($except_vita_16['sleep']<=1)
    {
      $except_vita_16['sleep']=1;
    }
    else if($except_vita_16['sleep']>=7)
    {
      $except_vita_16['sleep']=7;
    }
     if($except_vita_16['mindful']<=1)
    {
      $except_vita_16['mindful']=1;
    }
    else if($except_vita_16['mindful']>=7)
    {
      $except_vita_16['mindful']=7;
    }
     $data['stress_level'] = $except_vita_16['stress'];
     $data['sleep_level'] =$except_vita_16['sleep'];
     $data['mindfullness_level'] = $except_vita_16['mindful'];
     $data['vita_16_level']= round($vita_16);
	
     $stress_level= $except_vita_16['stress']*10/100;
     $sleep_level = $except_vita_16['sleep']*10/100;
     $mindfullness_level = $except_vita_16['mindful']*10/100;
     $vita_16_level= round($vita_16)*70/100;
     $data['total_mind_level'] =  round($vita_16_level+$stress_level+$sleep_level+$mindfullness_level); 
   
     $data['r_user_id'] = $userId;
     $data['test_date'] = $testdate;
   $result = $this->mind_switch_model->get_result($data['r_user_id']);
  // echo "count";
       // echo count($result);
     //   if(!empty($result) && count($result>=1))
       // {   
          $is_level_dt=$result->test_date;
          $id=$result->t_test_overall_result_level_id;
			$vita_level = $result->vita_16_level;          
       /*  if($c_date<=$is_level_dt)
            { */
				
     /**  if($vita_level=='' && $vita_level==NULL){*/
          $update_data=array('vita_16_level'=>$data['vita_16_level'],
                    'sleep_level'=>$data['sleep_level'],
                    'stress_level'=>$data['stress_level'],
                    'mindfullness_level'=>$data['mindfullness_level'],
                    'total_mind_level'=>$data['total_mind_level']
                    );
          $this->db->where('t_test_overall_result_level_id',$id);
          $this->db->update('t_test_overall_result_level',$update_data);
       /** }
        else{ 
           
              $data['type'] = 'followup';
              $this->mind_switch_model->insert_result($data);
            }
      }
        else{
          
          $data['type'] = 'first_consult';
          $this->mind_switch_model->insert_result($data);
        }
    ***/
    
  if($ins_id!='')
    {
     $msg = array('message'=>'Test is Succesfully inserted.' ,'status'=>'1');
    }
    else
    {
     $msg = array('error message'=>'Test is not inserted.' ,'status'=>'0');
    }
    echo json_encode($msg);
    unset($_SESSION['record_id']);
  
 }
 /***************to activate mind tab***************/
		public function activateMindTab()
		 {
			 $user_id = $this->input->post('user_id');
			 $data['coach_id'] = $this->input->post('coach_id');
			 $data['mind_status'] = 1;
			 $data['c_date'] =$create_date = date('Y-m-d');
			 $record = $this->mind_switch_model->get_mind_status($user_id,$create_date);
			 if(empty($record))
			 {
				 $data['user_id'] = $user_id;
				 //$record_date = $record->c_date;
				// if($create_date>$record_date){
				$insert_id = $this->mind_switch_model->save_mind_status($data);
				
				 if($insert_id!='')
				 {
					 $msg = "user is activated";
					 $status = 1;
					 $detail = $this->mind_switch_model->get_user_details($user_id);
					 
			  $subject = " Vragenlijsten ter voorbereiding op je consult.";
			  $link="http://shanethatech.com/testmv/moveqs/index.php?email=".$detail->coachemail."&fname=".$detail->first_name."&lname=".$detail->last_name;
			 $body = "<p style='font-family: verdana;'>Beste ".$detail->first_name.", <p><p style='font-family: verdana;'>Ik heb een vragenlijst voor je klaargezet waarin we voorafgaand aan het consult alvast een aantal thema's in kaart brengen. Het gaat om de thema's vitaliteit, slaap, stress en leven met aandacht. Je kunt deze vragen op jouw telefoon beantwoorden maar je kunt deze vragen ook op je tablet of computer beantwoorden. Onze ervaring is dat het overzichtelijker is om dit op de tablet of computer te doen. De uitkomsten van de vragenlijst zullen automatisch naar mij gestuurd worden en ik bespreek deze uitkomsten met je tijdens het consult. Klik om de vragenlijst te starten op de volgende <a href='".$link."' download>link</a>.</p> <p style='font-family: verdana;'>Met vriendelijke groet,</p><p style='font-family: verdana;'>Jouw Coach</p><p style='font-family: verdana;'>".$detail->coachfirst_name." ".$detail->coachlastname."</p>";
					 include APPPATH.'/phpmailer/PHPMailerAutoload.php';  
					  $mail = new PHPMailer();
					  $mail->SMTPDebug = true;  // debugging: 1 = errors and messages, 2 = messages only
					  $mail->SMTPAuth = true;  // authentication enabled
					  $mail->Host = 'mail.movesmart.offshoresolutions.nl';
					  $mail->Port = 25;
					  $mail->Username = 'movesmartinfo@movesmart.offshoresolutions.nl';
					  $mail->Password = 'welcome@108'; 
					  $mail->setFrom('movesmartinfo@movesmart.offshoresolutions.nl');
					  $mail->IsHTML(true); 
					  $mail->addAddress($detail->email);
					  //$mail->addAddress('sumitsingh.immanentsolutions@gmail.com');
						$mail->AddbCC('babitakapoor.immanentsolutions@gmail.com', 'Babita');
					  $mail->Subject = $subject;
					  $mail->msgHTML($body);
					   if (!$mail->send()) {
					$res = 'Mailer Error: '.$mail->ErrorInfo;
				}				
				 }
				 //}
			 }
			 else{
				 $status = $record->mind_status;
				 if($status == 1){
				 $msg = "user is already activated";
				 $status = 0;
				 }
				  else if($status == 0){
					 $data['mind_status'] = 1;
					 $this->db->where('user_id',$user_id);
					 $this->db->update('t_mind_activity',$data);
					 $msg = "user is activated";
					 $status = 1;
					 $detail = $this->mind_switch_model->get_user_details($user_id);
					  $subject = " Vragenlijsten ter voorbereiding op je consult.";
					  $link="http://shanethatech.com/testmv/moveqs/index.php?email=".$detail->coachemail."&fname=".$detail->first_name."&lname=".$detail->last_name;
					 $body = "<p style='font-family: verdana;'>Beste ".$detail->first_name.", <p><p style='font-family: verdana;'>Ik heb een vragenlijst voor je klaargezet waarin we voorafgaand aan het consult alvast een aantal thema's in kaart brengen. Het gaat om de thema's vitaliteit, slaap, stress en leven met aandacht. Je kunt deze vragen op jouw telefoon beantwoorden maar je kunt deze vragen ook op je tablet of computer beantwoorden. Onze ervaring is dat het overzichtelijker is om dit op de tablet of computer te doen. De uitkomsten van de vragenlijst zullen automatisch naar mij gestuurd worden en ik bespreek deze uitkomsten met je tijdens het consult. Klik om de vragenlijst te starten op de volgende <a href='".$link."' download>link</a>.</p> <p style='font-family: verdana;'>Met vriendelijke groet,</p><p style='font-family: verdana;'>Jouw Coach</p><p style='font-family: verdana;'>".$detail->coachfirst_name." ".$detail->coachlastname."</p>";
					 include APPPATH.'/phpmailer/PHPMailerAutoload.php';  
					  $mail = new PHPMailer();
					  $mail->SMTPDebug = true;  // debugging: 1 = errors and messages, 2 = messages only
					  $mail->SMTPAuth = true;  // authentication enabled
					  $mail->Host = 'mail.movesmart.offshoresolutions.nl';
					  $mail->Port = 25;
					  $mail->Username = 'movesmartinfo@movesmart.offshoresolutions.nl';
					  $mail->Password = 'welcome@108'; 
					  $mail->setFrom('movesmartinfo@movesmart.offshoresolutions.nl');
					  $mail->IsHTML(true); 
					  $mail->addAddress($detail->email);
						$mail->AddbCC('babitakapoor.immanentsolutions@gmail.com', 'Babita');
					  $mail->Subject = $subject;
					  $mail->msgHTML($body);
					   if (!$mail->send())
						   {
							$res = 'Mailer Error: '.$mail->ErrorInfo;
						}
				 }
			 }
			 echo json_encode(array('message'=>$msg,'status'=>$status));
			
			 
		 }
 
 /***************to check if  mind tab active***************/
		public function checkIsactive()
		 {
			 
			 $user_id = $this->input->post('user_id');
			 $record = $this->mind_switch_model->get_user_details($user_id);
			$tab_active_status = $record->mind_status;
			$client_email = $record->email;
			$client_first_name = $record->first_name;
			$client_last_name = $record->last_name;
			$coach_email = $record->coachemail;
			if($tab_active_status == 1)
			{
			$data['mindlink']="http://shanethatech.com/testmv/moveqs/index.php?email=".$coach_email."&fname=".$client_first_name."&lname=".$client_last_name;
				$status=1;
			
			}
			else{
				$data="tab is not active";
				$status = 0;
			}
			 echo json_encode(array('data'=>$data,'status'=>$status));
			
			 
		 }
		 public function mind_activities(){
			
			 $data['c_date'] = date("Y-m-d");
			 $data['user_id'] = $this->input->post('user_id');
			 $data['coach_id'] = $this->input->post('coach_id');
			 $result = $this->mind_switch_model->update_mind_activities($data);
				//echo '<pre>';print_r($result);
			if($result==1)
				{	
					$data = array('msg'=>'Success','status'=>1);
					
				}
				else if($result==0)
				{	
					$data = array('msg'=>'status not changed no record available','status'=>0,);					
				}
				echo json_encode($data);
		 }
}
