<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Strength extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        
        $this->load->model('strength_model');
		$this->load->model('report_model');
        $this->load->model('report_pdf_model');
      

    }
	public function index()
	{

		$situp_val=0;
		$pushup_val=0;
		$qua_val=0;	
		$situp_score=0;
		$pushup_score=0;
		$qua_score=0;

		$data= $this->input->post(NULL, TRUE);


	//	$test_option_type = $this->input->post('test_option_type');

		
	//	$type_option = json_decode($test_option_type);
	
	    $userId = $this->input->post('userId');
		$situp_val=$this->input->post('situp');
		$pushup_val=$this->input->post('pushup');
		$qua_val=$this->input->post('quadriceval');

		//$age = $this->input->post('age');
		//$gender = $this->input->post('gender');
		$user_detail = $this->report_pdf_model->user_detail($userId);
		$coach_id = $this->strength_model->get_coach_id($userId);
		
		$gender = $user_detail->gender;
		//for credits per activity//
			$age_weight_on_test = $this->report_pdf_model->user_age($userId);
			$age=$age_weight_on_test->age_on_test;
	
		
		$testdate = date('Y-m-d h:i:s');
		$c_date = date('Y-m-d h:i:s', strtotime('-3 month', strtotime($testdate)));
	
		
		
				$situp_score = $this->strength_model->get_strength_score($gender,$age,$situp_val,1);
			
				$pushup_score = $this->strength_model->get_strength_score($gender,$age,$pushup_val,2);

				$qua_score = $this->strength_model->get_strength_score($gender,$age,$qua_val,3);
			//echo $situp_score;
	
		$score = $situp_score.$pushup_score.$qua_score;
		
		$correct_level =$this->strength_model->get_strength_level($score);
		//echo $correct_level;
			$post_data = array('f_situpval' => $situp_val,
								'f_pushupval'=>$pushup_val,
								'f_quadriceval'=>$qua_val,
								'created_date' => $testdate,
								'r_user_id'=>$userId,
								'coach_id'=>$coach_id,
								'strength_auto_or_manual_calculation'=>$score
								);

					$is_record = $this->strength_model->check_result($userId);

					/*if(!empty($is_record))
					{
						$strength_id = $is_record->strength_user_test_id;
					$this->db->where('strength_user_test_id',$strength_id);
					//$this->db->where('r_user_id',$userId);
					$this->db->update('t_strength_user_test',$post_data);
					}							
					else{*/
						
						$ins_id = $this->strength_model->insert_data($post_data);
					//}
				///	$is_level = $this->strength_model->check_strength_level_record($userId,$c_date);
				//	if(!empty($is_level))
				//	{
				//		$is_level_dt=$is_level['dt'];
				//		$id=$is_level['id'];
				//		if($c_date<=$is_level_dt)
				//		{
						
				//		$up_data = array('strength_level' => $correct_level
										
				//						);
				//		$this->db->where('t_user_strength_level_id',$id);
				//	$this->db->update('t_user_strength_level',$up_data);
				
				//	$msg = array('message'=>'Test is Succesfully updated.' ,'status'=>'10');
				//	}
					//else{
						
						$in_data = array('strength_level' => $correct_level,
								'date' => $testdate,
								'r_user_id'=>$userId
								);
						$insert_id = $this->strength_model->insert_data_strength($in_data);
						
						$msg = array('message'=>'Test is Succesfully inserted.' ,'status'=>'11');
				//	}
				//	}
					//else{
						
					/*	$in_data = array('strength_level' => $correct_level,
								'date' => $testdate,
								'r_user_id'=>$userId
								);
						$insert_id = $this->strength_model->insert_data_strength($in_data);
						$msg = array('message'=>'Test is Succesfully inserted.' ,'status'=>'11');
					}*/
					echo json_encode($msg);
	}
	public function getstre()
	{   
		$userId=$this->input->post('userId');
		$data['strength']= $this->strength_model->check_result($userId);
		echo json_encode($data);
	}
	
}
	