<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Saveprogram_model extends CI_Model 
 {
	 
 	public function SaveStatus($data)
	{

		$result = $this->db->insert('t_save_strength_program',$data);
			$insert_id = $this->db->insert_id();

		return  $insert_id;
		
	}

	public function check_record($program_id,$user_id,$machine_id,$tr_date){
		
			$this->db->select('*');
			$this->db->from('t_save_strength_program');
			$this->db->where('user_id', $user_id);
			$this->db->where('program_id', $program_id);
			$this->db->where('machine_id', $machine_id);
			$this->db->where('training_date', $tr_date);
			$this->db->order_by('id','desc');
			$query = $this->db->get();
			$result = $query->row();
				
			if($query->num_rows()>0){
				//print_r($result);
				return $result;
			}
			else{
				return false;
			}
		
	}
	
	
	public function get_week($user_id){
		
		$this->db->select('*');
			$this->db->from('t_strength_user_test_week');
			$this->db->where('r_user_id', $user_id);
			$query = $this->db->get();
			$result = $query->row();
				
			if($query->num_rows()>0){
				//print_r($result);
				return $result;
			}
			else{
				return false;
			}
		
		
	}
	public function check_training($user_id,$tr_date)
	{
		$this->db->select('*');
			$this->db->from('t_strength_user_test_week');
			$this->db->where('r_user_id', $user_id);
			$this->db->where('week_start_date <=', $tr_date);
			$this->db->where('week_end_date >=', $tr_date);
			$query = $this->db->get();
			$result = $query->row();
				
			if($query->num_rows()>0){
				//print_r($result);
				return $result;
			}
			else{
				return false;
			}
	}
		public function machineDetails($result){

			$this->db->select('strength_machine_id,machine_name,videourl,machineimage');
			$this->db->from('t_strength_machine');
			$this->db->where('is_deleted',0);
			
				$query = $this->db->get();
				$result = $query->result();
					
					if(!empty($result)){
					
						foreach($result as $res){
							$machine_id = $res->strength_machine_id;
							$res->weight = $this->getblock_weight($machine_id);
							$data[] = $res;
						}
						return $data;
						
					}else {
					return false;
				}
			}
			

		
		public function getblock_weight($machine_id){
			
			$query = "select * from t_bolck_weight where strength_machine_id=".$machine_id;
			$result = $this->db->query($query);
			$result = $result->result();
			if(!empty($result)){
				return $result;
			}
			else{
				return $result = array();
			}
				
		}



} 


?>