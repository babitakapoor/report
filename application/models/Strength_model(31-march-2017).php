<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Strength_model extends CI_Model {
/***********for mind switch insertion api*******/
public function insert_data($post_data)
{
	 $this->db->insert('t_strength_user_test', $post_data);
   $insert_id = $this->db->insert_id();

   return  $insert_id;
	
}
public function insert_data_strength($in_data)
{
	 $this->db->insert('t_user_strength_level', $in_data);
   $insert_id = $this->db->insert_id();

   return  $insert_id;
	
}
public function check_result($userId)
{
	 $this->db->select('*');
	 $this->db->from('t_strength_user_test');
	 $this->db->where('r_user_id',$userId);
	 $this->db->order_by('strength_user_test_id','DESC');
	$query = $this->db->get();
	$result = $query->row();
	if(!empty($result))
	{
		return $result;
	}
	else
	{
		return false;
	}
	
}
public function get_strength_score($gender,$age,$val,$type)
{
	 $this->db->select('r_level');
	 $this->db->from('t_strength_level');
	 $this->db->where('r_gender',$gender);
	 $this->db->where('r_type',$type);
	 $this->db->where('age_min <=',$age);
	$this->db->where('age_max >',$age);
	$this->db->where('f_min <=',$val);
	$this->db->where('f_max >=',$val);
	$query = $this->db->get();
	$result = $query->result();
	if(!empty($result))
	{
		
			
		return $result[0]->r_level;
		
		
	}
	else
	{
		return false;
	}
	
}
public function get_strength_level($score)
	{
		 $this->db->select('level');
		 $this->db->from('t_strength_correct_level');
		// $this->db->like('fscore',$score);
		$this->db->like('score', $score);
		$query = $this->db->get();
		$result = $query->result();

		if(!empty($result))
		{
			foreach($result as $score_level)
			{
			return $score_level->level;
			}
		}
		else
		{
			return false;
		}
		
	}
public function check_strength_level_record($user_id,$date)
	{
			 $this->db->select('*');
			 $this->db->from('t_user_strength_level');
			 $this->db->where('r_user_id',$user_id);
			 $this->db->order_by('t_user_strength_level_id','desc');
			$query = $this->db->get();
			$result = $query->result();
			if(!empty($result))
			{
				$data['dt'] = $result[0]->date;
				$data['id'] = $result[0]->t_user_strength_level_id;
				
				return $data;
			}
			else
			{
				return false;
			}
		
	}
public function get_coach_id($userId)
{
	$this->db->select('r_coach_id');
	$this->db->from('t_coach_member');
	$this->db->where('r_user_id',$userId);
	$query = $this->db->get();
	$result = $query->row();

	if(!empty($result)){
		return $result->r_coach_id;
	}
	else{
		return false;
	}
	
	
	
}

}
?>